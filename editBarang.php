<?php
include('koneksi/conn.php');
include('header.php');


$brg_id = $_POST["brg_id"];

$sql = "SELECT * FROM barang where brg_status = 1 and brg_id = '$brg_id'";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) { 
        
        $brg_id = $row["brg_id"];  
        $brg_barcode = $row["brg_barcode"];  
        $brg_satuan = $row["brg_satuan"];  
        $brg_nama = $row["brg_nama"];  
        $brg_kode = $row["brg_kode"];  
        $brg_hargajual = $row["brg_hargajual"];  
        $brg_stok = $row["brg_stok"];  

      }
  }

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Barang
        <small>barang</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Edit Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Barang</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="barang/input.php" method="POST">
              <div class="box-body">
                <input type="hidden" value="<?php echo $brg_id; ?>" name="brg_id">
                <div class="form-group">
                  <label for="exampleInputPassword1">Kode Barang  <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="brg_kode" id="exampleInputPassword1" value="<?php echo $brg_kode; ?>" placeholder="Masukkan Kode Barang" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Nama Barang  <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="brg_nama" id="exampleInputPassword1" value="<?php echo $brg_nama; ?>" placeholder="Masukkan Nama Barang" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Satuan  <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="brg_satuan" value="<?php echo $brg_satuan; ?>" id="exampleInputPassword1" placeholder="Masukkan Satuan Barang"  required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Barcode <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="brg_barcode" value="<?php echo $brg_barcode; ?>" id="exampleInputPassword1" placeholder="Masukkan Barcode Barang" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Harga   <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="brg_hargajual" value="<?php echo $brg_hargajual; ?>" id="exampleInputPassword1" placeholder="Masukkan Harga Barang" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Stok  <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="brg_stok" value="<?php echo $brg_stok; ?>" id="exampleInputPassword1" placeholder="Masukkan Stok Barang" required >
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

<script>
$(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    })
})


function cekkota() {
   var cli_prov = $("#cli_prov").val();
//    alert (cli_prov);
   $("#cli_kota").load("master/kota.php?cli_prov="+cli_prov);
}
</script>

<?php
include('footer.php');
?>
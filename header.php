<?php
session_start();
error_reporting(0);
$url = "http://localhost/projek/";
if (empty($_SESSION['fullname'])) {
    header($url."login.php");
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Interactive</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta
            content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
            name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link
            rel="stylesheet"
            href="<?php echo $url;?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link
            rel="stylesheet"
            href="<?php echo $url;?>assets/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link
            rel="stylesheet"
            href="<?php echo $url;?>assets/bower_components/Ionicons/css/ionicons.min.css">
        <!-- DataTables -->
        <link
            rel="stylesheet"
            href="<?php echo $url;?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo $url;?>assets/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of
        downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo $url;?>assets/dist/css/skins/_all-skins.min.css">
                <!-- jQuery 3 -->
        <script src="<?php echo $url;?>assets/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo $url;?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="../../index2.html" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">
                        <b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">
                        <b>Admin</b>LTE</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img
                                        src="<?php echo $url;?>assets/dist/img/user2-160x160.jpg"
                                        class="user-image"
                                        alt="User Image">
                                    <span class="hidden-xs"><?php 

print_r($_SESSION['fullname']); ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img
                                            src="<?php echo $url;?>assets/dist/img/user2-160x160.jpg"
                                            class="img-circle"
                                            alt="User Image">

                                        <p>
                                            Alexander Pierce - Web Developer
                                            <small>Member since Nov. 2012</small>
                                        </p>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="base/logout.php" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img
                    src="<?php echo $url;?>assets/dist/img/user2-160x160.jpg"
                    class="img-circle"
                    alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php 

                print_r($_SESSION['fullname']); ?></p>
                <a href="#">
                    <i class="fa fa-circle text-success"></i>
                    Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="../../index.html">
                            <i class="fa fa-circle-o"></i>
                            Dashboard v1</a>
                    </li>
                    <li>
                        <a href="../../index2.html">
                            <i class="fa fa-circle-o"></i>
                            Dashboard v2</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i>
                    <span>Employee</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo $url;?>inputEmployee.php">
                            <i class="fa fa-circle-o"></i>
                            Input Employee</a>
                    </li>
                    <li>
                        <a href="<?php echo $url;?>indexemployee.php">
                            <i class="fa fa-circle-o"></i>
                            Data Employee</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i>
                    <span>Customer</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo $url;?>inputCostumer.php">
                            <i class="fa fa-circle-o"></i>
                            Input Customer</a>
                    </li>
                    <li>
                        <a href="<?php echo $url;?>index.php">
                            <i class="fa fa-circle-o"></i>
                            Data Customer</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i>
                    <span>Barang</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo $url;?>inputBarang.php">
                            <i class="fa fa-circle-o"></i>
                            Input Barang</a>
                    </li>
                    <li>
                        <a href="<?php echo $url;?>indexBarang.php">
                            <i class="fa fa-circle-o"></i>
                            Data Barang</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i>
                    <span>Transaksi</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo $url;?>transaksi/inputTran.php">
                            <i class="fa fa-circle-o"></i>
                            Input Transaksi</a>
                    </li>
                    <li>
                        <a href="<?php echo $url;?>transaksi/indexTran.php">
                            <i class="fa fa-circle-o"></i>
                            Data Transaksi</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i>
                    <span>Rekap</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo $url;?>rekap/indexRekap.php">
                            <i class="fa fa-circle-o"></i>
                            Lihat Rekap</a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>


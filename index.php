<?php
include('koneksi/conn.php');
include('header.php');

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Manajemen Data Costumer
            <small>Data Costumer</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Home</a>
            </li>
            <li>
                <a href="#">Tables</a>
            </li>
            <li class="active">Data Costumer</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Costumer</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Perusahaan</th>
                                    <th>Bidang Usaha</th>
                                    <th>Alamat</th>
                                    <th>Kota</th>
                                    <th>Telepon</th>
                                    <th>NPWP</th>
                                    <th>Edit</th>
                                    <th>Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sql = "SELECT * FROM client where cli_status = 1";
                                $result = $conn->query($sql);
                                $i=1;
                                if ($result->num_rows > 0) {
                                    while($row = $result->fetch_assoc()) { ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $row["cli_nama"];?></td>
                                    <td><?php echo $row["cli_perusahaan"];?></td>
                                    <td><?php echo $row["cli_bidang"];?></td>
                                    <td><?php echo $row["cli_alamat"];?></td>
                                    <td><?php echo $row["cli_kota"];?></td>
                                    <td><?php echo $row["cli_telp"];?></td>
                                    <td><?php echo $row["cli_npwp"];?></td>
                                    <td>
                                        <?php
                                        if($row["cli_employee"] == $_SESSION['id_employee'] || $_SESSION['level'] == 1){ ?>
                                        <form action="updatecostumer.php" method="post">
                                            <input type="hidden" name="cli_id" value="<?php echo $row["cli_id"];?>">
                                            <button type="submit" class="btn btn-warning">Edit</button>
                                        </td>
                                        <?php
                                        }
                                        ?>
                                        
                                    </form>
                                    <td>
                                    <?php
                                        if($row["cli_employee"] == $_SESSION['id_employee'] || $_SESSION['level'] == 1){ ?>

                                            <a href="javascript:hapus('<?php echo $row["cli_id"];?>');" class="btn btn-danger">hapus</a>
                                    <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>


<script>
$(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    })
})

function hapus(id) {

    var msg = confirm("Apakah Anda yakin dihapus?");
    if (msg == true) {
        window.location = "costumer/hapus.php?cli_id="+id;
    }
}
</script>

<?php
include('footer.php');
?>
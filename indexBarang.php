<?php
include('koneksi/conn.php');
include('header.php');

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Manajemen Data Barang
            <small>Data Barang</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Home</a>
            </li>
            <li>
                <a href="#">Tables</a>
            </li>
            <li class="active">Data Barang</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Costumer</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Stok</th>
                                    <th>Barcode</th>
                                    <th>Harga</th>
                                    <th>Edit</th>
                                    <th>Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sql = "SELECT * FROM barang where brg_status = 1";
                                $result = $conn->query($sql);
                                $i=1;
                                if ($result->num_rows > 0) {
                                    while($row = $result->fetch_assoc()) { ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $row["brg_nama"];?></td>
                                    <td><?php echo $row["brg_stok"]." ".$row["brg_satuan"];?></td>
                                    <td><?php echo $row["brg_barcode"];?></td>
                                    <td><?php echo "Rp " . number_format($row["brg_hargajual"],2,',','.');?></td>
                                    <td>
                                        <form action="editbarang.php" method="post">
                                            <input type="hidden" name="brg_id" value="<?php echo $row["brg_id"];?>">
                                            <button type="submit" class="btn btn-warning">Edit</button>
                                        </td>
                                    </form>
                                    <td>
                                        <a href="javascript:hapus('<?php echo $row["brg_id"];?>');" class="btn btn-danger">hapus</a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<script>
$(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    })
})

function hapus(id) {

    var msg = confirm("Apakah Anda yakin dihapus?");
    if (msg == true) {
        window.location = "barang/hapus.php?brg_id="+id;
    }
}
</script>

<?php
include('footer.php');
?>
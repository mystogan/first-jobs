<?php
include('koneksi/conn.php');
include('header.php');

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Manajemen Data Karyawan
            <small>Data Karyawan</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Home</a>
            </li>
            <li>
                <a href="#">Tables</a>
            </li>
            <li class="active">Data Karyawan</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Karyawan</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Lengkap</th>
                                    <th>Jabatan</th>
                                    <th>Lokasi Kantor</th>
                                    <th>Edit</th>
                                    <th>Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sql = "SELECT * FROM employee 
                                        join lokasi on lok_id = emp_idlokasi
                                        where emp_status = 1";
                                $result = $conn->query($sql);
                                $i=1;
                                if ($result->num_rows > 0) {
                                    while($row = $result->fetch_assoc()) { ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $row["emp_fullname"];?></td>
                                    <td><?php 
                                    if($row["emp_jabatan"] == 1){
                                        echo "Maketing";
                                    }else {
                                        echo "Customer Support";
                                    }
                                    ?></td>
                                    <td><?php echo $row["lok_nama"];?></td>
                                    <td>
                                        <form action="updateEmployee.php" method="post">
                                            <input type="hidden" name="emp_id" value="<?php echo $row["emp_id"];?>">
                                            <button type="submit" class="btn btn-warning">Edit</button>
                                        </td>
                                    </form>
                                    <?php
                                    if ($row['emp_level'] == 2) { ?>
                                    <td>
                                        <a href="javascript:hapus('<?php echo $row["emp_id"];?>');" class="btn btn-danger">hapus</a>
                                    </td>                                        
                                    <?php
                                    }else { ?>
                                    <td>Administrator
                                    </td>

                                    <?php
                                    }
                                    ?>
                                </tr>
                                <?php
                                $i++;
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
$(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    })
})

function hapus(id) {

    var msg = confirm("Apakah Anda yakin dihapus?");
    if (msg == true) {
        window.location = "employee/hapus.php?cli_id="+id;
    }
}
</script>

<?php
include('footer.php');
?>
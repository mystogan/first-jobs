<?php
include('koneksi/conn.php');
include('header.php');

?>

<link
    rel="stylesheet"
    href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css">
<script src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            General Form Elements
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Home</a>
            </li>
            <li>
                <a href="#">Forms</a>
            </li>
            <li class="active">General Elements</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Quick Example</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="costumer/input.php" method="POST">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tahu Dari</label>

                                <select name="cli_from" id="cli_from" class="form-control">
                                    <option value="1">Kebetulan Jalan-Jalan</option>
                                    <option value="2">Dimedsos</option>
                                    <option value="3">Dari Teman</option>
                                    <option value="4">Dari Brosur</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tanggal Customer Gabung
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="date"
                                    class="form-control"
                                    name="cli_gabung"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan Gabungnya Costumer"
                                    required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Nama Customer
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    name="cli_nama"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan nama Costumer"
                                    required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Perusahaan customer
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    name="cli_perusahaan"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan Perusahaan Costumer"
                                    required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Bidang Customer
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    name="cli_bidang"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan Bidang Costumer"
                                    required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">NPWP Customer
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    name="cli_npwp"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan NPWP Costumer"
                                    required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Alamat Customer
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    name="cli_alamat"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan Alamat Costumer"
                                    required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Provinsi Customer</label>
                                <select
                                    name="cli_prov"
                                    id="cli_prov"
                                    class="form-control"
                                    onchange="javascript:cekkota();">
                                    <?php
                                    $sql = "SELECT * FROM provinces ";
                                    $result = $conn->query($sql);
                                    $i=1;
                                    if ($result->num_rows > 0) {
                                        while($row = $result->fetch_assoc()) { ?>
                                    <option value="<?php echo $row["id"];?>"><?php echo $row["name"];?></option>

                                    <?php
                                    $i++;
                                        }
                                    }
                                    ?>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Kota Customer</label>
                                <select name="cli_kota" id="cli_kota" class="form-control">
                                    <option value="0">Silahkan Pilih Provinsi</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Telepon Customer
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    name="cli_telp"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan Telepon Costumer"
                                    required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">HP Customer</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    name="cli_hp"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan HP Costumer">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Email Customer</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    name="cli_email"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan Email Costumer">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">PIN BB Customer</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    name="cli_pinBB"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan Pin BB Costumer">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Atasan Customer</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    name="cli_atasan"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan Atasan Costumer">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">IT Consultan Customer</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    name="cli_itconsultan"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan IT Consultan Costumer">
                            </div>
                        </div>
                        <?php 
                        if($_SESSION['level'] == 1){ ?>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Nama Employee Yang Memasukkan
                                <span style="color:red;">*</span></label><br>
                            <select id="example" class="form-control" name="cli_employee">

                                <?php
                                  $sql = "SELECT * FROM employee where emp_status = 1";
                                  $result = $conn->query($sql);
                                  $i=1;
                                  if ($result->num_rows > 0) {
                                      while($row = $result->fetch_assoc()) { ?>
                                <option value="<?php echo $row["emp_id"];?>"><?php echo $row["emp_fullname"];?></option>

                                <?php
                                  $i++;
                                      }
                                  }
                                  ?>
                            </select>
                            <script>
                                $('#example').select2({placeholder: 'Select a month'});
                            </script>
                            <!-- <input type="text" class="form-control" style="width:30%" name="tran_idcli"
                            id="exampleInputPassword1" required="required"> -->
                        </div>
                        <?php
                        }
                        ?>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })

    function cekkota() {
        var cli_prov = $("#cli_prov").val();
        //    alert (cli_prov);
        $("#cli_kota").load("master/kota.php?cli_prov=" + cli_prov);
    }
</script>

<?php
include('footer.php');
?>
<?php
include('koneksi/conn.php');
include('header.php');

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Input Employee
        <small>Input</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Input Employee</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Employee</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="employee/input.php" method="POST">
            <input type="hidden" name="emp_id" value="">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Lengkap Employee <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="emp_fullname" id="exampleInputEmail1" placeholder="Masukkan Nama Lengkap Peggawai" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Panggilan Employee  <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="emp_shortname" id="emp_shortname" placeholder="Masukkan nama Panggilan Pegawai" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Jenis Kelamin Employee</label>
                  <select name="emp_jk" id="emp_jk" class="form-control">
                    <option value="1">Laki-Laki</option>
                    <option value="2">Perempuan</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Kota Kelahiran Employee  <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="emp_kotalhr" id="emp_kotalhr" placeholder="Masukkan Kota Kelahiran"  required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Tanggal Lahir Employee  <span style="color:red;">*</span></label>
                  <input type="date" class="form-control" name="emp_tgllhr" id="emp_tgllhr" placeholder="Masukkan tanggal Lahir" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Alamat KTP  <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="emp_alamatktp" id="emp_alamatktp" placeholder="Masukkan Alamat KTP" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Domisili  <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="emp_domisili" id="emp_domisili" placeholder="Masukkan Domisili" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Hunian</label>
                  <input type="text" class="form-control" name="emp_hunian" id="emp_hunian" placeholder="Masukkan Hunian">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Status Nikah</label>
                  <select name="emp_statusnikah" id="emp_statusnikah" class="form-control">
                    <option value="1">Sudah Menikah</option>
                    <option value="2">Belum Menikah</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Jumlah Anak</label>
                  <select name="emp_anak" id="emp_anak" class="form-control">
                    <option value="0">0 Anak</option>
                    <option value="1">1 Anak</option>
                    <option value="2">2 Anak</option>
                    <option value="3">3 Anak</option>
                    <option value="4">4 Anak</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Telepon Employee</label>
                  <input type="text" class="form-control" name="emp_tlp" id="emp_tlp" placeholder="Masukkan Telefon">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">No HP 1 Employee</label>
                  <input type="text" class="form-control" name="emp_hp1" id="emp_hp1" placeholder="Masukkan HP 1">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">No HP 2 Employee</label>
                  <input type="text" class="form-control" name="emp_hp2" id="emp_hp2" placeholder="Masukkan HP 2">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">No HP 3 Employee</label>
                  <input type="text" class="form-control" name="emp_hp3" id="emp_hp3" placeholder="Masukkan HP3">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">PIN BB Employee</label>
                  <input type="text" class="form-control" name="emp_pinbb" id="emp_pinbb" placeholder="Masukkan Pin BB">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Email Employee</label>
                  <input type="email" class="form-control" name="emp_email" id="emp_email" placeholder="Masukkan Email">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Devisi</label>
                  <select name="emp_devisi" id="emp_devisi" class="form-control">
                    <option value="1">Maketing</option>
                    <option value="2">Customer Support</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Lokasi</label>
                  <select name="emp_idlokasi" id="emp_idlokasi" class="form-control">
                  <?php
                    $sql = "SELECT * FROM lokasi where lok_status = 1";
                    $result = $conn->query($sql);
                    $i=1;
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) { ?>
                        <option value="<?php echo $row["lok_id"];?>"><?php echo $row["lok_nama"];?></option>
                    
                    <?php
                    $i++;
                        }
                    }
                    ?>

                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Jabatan Employee</label>
                  <input type="text" class="form-control" name="emp_jabatan" id="emp_jabatan" placeholder="Masukkan jabatan">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Level</label>
                  <select name="emp_level" id="emp_level" class="form-control">
                    <option value="1">Administrator</option>
                    <option value="2">Biasa</option>
                  </select>
                </div>
                
                <div class="form-group">
                  <label for="exampleInputFile">Username <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="log_username" id="log_username" placeholder="Masukkan Usermame" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Password <span style="color:red;">*</span></label>
                  <input type="password" class="form-control" name="log_password" id="log_password" placeholder="Masukkan Password" required >
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

<script>
$(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    })
})
</script>

<?php
include('footer.php');
?>
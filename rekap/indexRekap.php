<?php
// file digunakan untuk search dan melihat rekap transaksi
// created_date : 2019-01-03
// updated_date : 2019-01-04

include('../koneksi/conn.php');
include('../header.php');

// untuk menangkap masing-masing form yang akan dicari 
$dateawal = $_POST["dateAwal"]; // tanggal awal
$dateakhir = $_POST["dateAkhir"]; // tanggal akhir
$kode_transaksi = $_POST["kode"]; // kode transaksi jika ada

// kondisi dimana kode transaksi adalah null akan diset menjadi tidak ada isinya
if($kode_transaksi == null)
{
    $kode_transaksi = '';
}else 
{
    // variabel yang akan digunakan untuk menyimpan apakah ada kata yang rawan dalam database 
    $b = 0;
    // berfungsi untuk memecah inputan user menjadi array 
    $simpan = explode(" ",$kode_transaksi);
    // perulangan untuk mengecek ada kata yang rawan dalam queri atau tidak 
    for ($i=0; $i < count($simpan); $i++) 
    {   
        // berfungsi untuk mengubah semua huruf menjadi kecil sehingga bisa rata semua 
        $cek = trim(strtolower($simpan[$i]));
        // kondisi dimana digunakan untuk mengecek apakah ada kata dalam array yang rawan dalam query atau tidak 
        if($cek == "select" || $cek == "delete" || $cek == "update" || $cek == "show" || $cek == "database" || $cek == "insert" )
        {
            $b = 1;
            // ketika ada kata yang rawan maka proses perulangan akan langsung dihentikan 
            break;
        }    
    }
}
// kondisi dimana ada kata rawan dari inputan, kemudian akanditampilakn pesan 'terdapatkata yang terlarang'
if ($b == 1) {
    // mengset inputan kembali pada null karena ada kata pada inputan yang rawan 
    $kode_transaksi = '';
    // menampilkan pesan pada customer karena ada kata yang rawan pada inputan 
    echo "<script>alert ('Terdapat Kata yang dilarang yaitu $cek');</script>";
}

// kondisi dimana ketika tanggal tidak dimasukkan atau ketika pertama kali mengklik akan diset oleh tanggal sekarang dan 
//          1 bulan kedepan
if($dateawal == null)
{
    $dateawal = date('Y-m-d'); // proses set tanggal sekarang
    $dateakhir = date('Y-m-d', strtotime('+1 month', strtotime($dateawal))); // proses set tanggal sekarang ditambah 1 bulan
}else 
{
    // kondisi dimana ketika input tanggal tidak valid yaitu tanggal akhir lebih kecil dari pada tanggal awal maka akan 
    //              di set kembali tanggal sekarang dan akan muncul pesan untuk memasukkan tanggal yang valid
    if($dateakhir < $dateawal)
    {
        echo "<script>alert ('Maaf Tanggal Tidak Valid Silahkan Masukkan Tanggal Akhir > Tanggal Awal');</script>";
        $dateawal = date('Y-m-d');
        $dateakhir = date('Y-m-d', strtotime('+1 month', strtotime($dateawal)));
    }
}

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Rekap
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Home</a>
            </li>
            <li>
                <a href="#">Rekap</a>
            </li>
            <li class="active">Lihat rekap</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Rekap</h3>
                    </div>
                    <div class="col-md-12">
                        <form action="<?php echo $url;?>rekap/indexRekap.php" method="post">
                            <div class="col-md-3">
                            <label for="">Tanggal Mulai</label>
                                <input type="date" class="form-control" name="dateAwal" value="<?php echo $dateawal;?>">
                            </div>
                            <div class="col-md-3">
                                <label for="">Tanggal Akhir</label>
                                <input type="date" class="form-control" name="dateAkhir" value="<?php echo $dateakhir;?>">
                            </div>
                            <div class="col-md-3">
                                <label for="">Masukkan kode Transaksi</label>
                                <input type="text" class="form-control" name="kode" placeholder="Masukkan Kode Transaksi" value="<?php echo $kode_transaksi;?>">
                            </div>
                            <div class="col-md-3">
                            <br>
                                <button type="submit" class="btn btn-primary">Cari</button>
                            </div>
                        </form>
                        
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <hr>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Kode Transaksi</th>
                                    <th>Sub Total</th>
                                    <th>Diskon</th>
                                    <th>Grand Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                // query untuk menampilkan rekap data transaksi 
                                $sql = "SELECT *, DATE_FORMAT(tran_date, '%d-%m-%Y') as tanggal
                                        FROM transaksi
                                        join client on cli_id = tran_idcli
                                        where cli_status = 1 
                                        and ( tran_date >= '$dateawal'
                                        and tran_date <= '$dateakhir' )";
                                if($kode_transaksi != null){
                                    $sql = $sql." and tran_kode like '%$kode_transaksi%'";   
                                }
                                // echo $sql ;
                                $result = $conn->query($sql);
                                $i=1;
                                $sumtotal = 0;
                                $sumdiskon = 0;
                                $sumGrand = 0;
                                if ($result->num_rows > 0) {
                                    while($row = $result->fetch_assoc()) { ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $row["tanggal"];?></td>
                                    <td><?php echo $row["tran_kode"];?></td>
                                    <td style="text-align: right;"><?php echo "Rp " . number_format($row["tran_subtotal"],2,',','.');?></td>
                                    <td style="text-align: right;"><?php echo "Rp " . number_format($row["tran_diskon"],2,',','.');?></td>
                                    <td style="text-align: right;"><?php echo "Rp " . number_format($row["tran_grandtotal"],2,',','.');?></td>
                                </tr>
                                <?php
                                // proses menambahkan total untukj di sum
                                $sumtotal = $sumtotal +$row["tran_subtotal"];
                                // proses menjumlahkan diskon yang akan ditampilkan 
                                $sumdiskon = $sumdiskon +$row["tran_diskon"];
                                // proses perhitungan semua grandtotal yang akan ditampilkan
                                $sumGrand = $sumGrand +$row["tran_grandtotal"];
                                $i++;
                                    }
                                }
                                ?>
                                <tr>
                                    <td colspan="3" style="font-weight: bold;">Jumlah</td>
                                    <td style="font-weight: bold; text-align: right;"><?php echo "Rp " . number_format($sumtotal,2,',','.');?></td>
                                    <td style="font-weight: bold; text-align: right;"><?php echo "Rp " . number_format($sumdiskon,2,',','.');?></td>
                                    <td style="font-weight: bold; text-align: right;"><?php echo "Rp " . number_format($sumGrand,2,',','.');?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })

    function hapus(id) {
        var msg = confirm("Apakah Anda yakin dihapus?");
        if (msg == true) {
            window.location = "hapus.php?tran_id=" + id;
        }
    }
</script>

<?php
include('../footer.php');
?>
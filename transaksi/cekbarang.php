<?php
// file ini digunakan untuk mengecek barang dan harga barang serta mengirimkan data brang yang digunakan untuk 
// menentukan harga barang yang akan diklik 

// created_date : 2018-12-28
// updated_date : 2019-01-03

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "projek";

// 
$conn = new mysqli($servername, $username, $password, $dbname);

// dalam hal ini digunakan untuk mengganti atau mereplace tanda petik untuk menghindari query error
$brg_id = str_replace("'","''",$_GET["brg_id"]);   

// * query yang akan digunakan dalam file ini
$sql = "SELECT * 
        FROM barang
        where brg_id = '$brg_id' ;";
// * eksekusi query yang akan digunakan dalam file
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $data["brg_barcode"] = $row["brg_barcode"];
        $data["brg_satuan"] = $row["brg_satuan"];
        $data["brg_nama"] = $row["brg_nama"];
        $data["brg_kode"] = $row["brg_kode"];
        $data["brg_hargajual"] = $row["brg_hargajual"];
        $data["brg_stok"] = $row["brg_stok"];
    }
} else {

}

// * return yang berupa json yang digunakan untuk mengirim data barang dan mengset harga barang di inputtrans.php 
print_r (json_encode($data));
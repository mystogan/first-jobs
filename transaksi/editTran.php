<?php
// file ini berfungsi untuk form mengedit transaksi 
// created_date : 2018-12-28
// update_date : 2019-01-03

include('../koneksi/conn.php');


// berfungsi untuk mendapatkan method POST yang telah dikirim oleh form 
$tran_id = $_POST["tran_id"];

// query yang digunakan untuk mendapatkan data dari tabel transaksi pada id tertentu 
$sql = "SELECT * FROM transaksi where tran_id = '$tran_id'";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {         
        $tran_idcli = $row["tran_idcli"];  
        $tran_kode = $row["tran_kode"];  
        $tran_date = $row["tran_date"];  
        $tran_diskon = $row["tran_diskon"];  

      }
  }

include('../header.php');
?>
<!-- digubnakan untk dropdown -->
<link
    rel="stylesheet"
    href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css">
<script src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Input Transaksi
            <small>barang</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Home</a>
            </li>
            <li>
                <a href="#">Forms</a>
            </li>
            <li class="active">Input Transaksi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Input Transaksi</h3>
                    </div>
                    <form role="form" action="updatetran.php" method="POST">
                        <input type="hidden" name="tran_id" value="<?php echo $tran_id;?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nama Customer
                                    <span style="color:red;">*</span></label><br>
                                <select id="example" class="form-control" style="width:30%" name="tran_idcli">

                                    <?php
                                  $sql = "SELECT * FROM client where cli_status = 1";
                                  $result = $conn->query($sql);
                                  $i=1;
                                  if ($result->num_rows > 0) {
                                      while($row = $result->fetch_assoc()) { ?>
                                    <option value="<?php echo $row["cli_id"];?>" <?php if($row["cli_id"] == $tran_idcli){ echo "selected"; }?> ><?php echo $row["cli_nama"];?></option>

                                    <?php
                                  $i++;
                                      }
                                  }
                                  ?>
                                </select>
                                <script>
                                    $('#example').select2({placeholder: 'Select a month'});
                                </script>
                                <!-- <input type="text" class="form-control" style="width:30%" name="tran_idcli"
                                id="exampleInputPassword1" required="required"> -->
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Kode Transaksi
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    style="width:30%"
                                    name="tran_kode"
                                    id="exampleInputPassword1"
                                    required="required"
                                    value="<?php echo $tran_kode?>"
                                    >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Tanggal Transaksi
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="date"
                                    class="form-control"
                                    style="width:30%"
                                    name="tran_date"
                                    id="exampleInputPassword1"
                                    value="<?php echo $tran_date?>"
                                    placeholder="Masukkan Satuan Barang"
                                    required="required">
                            </div>
                            
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- page script -->
<?php
include('../footer.php');
?>

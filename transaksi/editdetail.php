<?php
// file ini digunakan untuk mengubah transaksi atau edit transaksi 

// created_date : 2018-12-28
// updated_date : 2019-01-03

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "projek";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// * berikut adalah fungsi php yang digunakan untuk mengganti atau merubah tanda petik yang akan dimasukkan dalam query 
$tran_id = str_replace("'","''",$_POST["tran_id"]);
$dt_id = str_replace("'","''",$_POST["dt_id"]);
$dt_id_barang = str_replace("'","''",$_POST["dt_id_barang"]);
$dt_diskripsi = str_replace("'","''",$_POST["dt_diskripsi"]);
$dt_harga = str_replace("'","''",$_POST["dt_harga"]);
$dt_kuantitas = str_replace("'","''",$_POST["dt_kuantitas"]);
$dt_kuantitas_lama = str_replace("'","''",$_POST["dt_kuantitas_lama"]);
// * fungsi ini digunakan untuk menghitung jumlah yang ada 
$dt_jumlah = $dt_kuantitas * $dt_harga;

// * query yang digunakan untuk mengecek stok barang apakah cukup atau tidak 
$op = 1;
$sql = "SELECT * FROM barang where brg_status = 1 and brg_id = '$dt_id_barang'; ";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) { 
        $brg_stok = $row['brg_stok']+$dt_kuantitas_lama;
        if($brg_stok < $dt_kuantitas ){
            $pesan = "Stok Tidak Cukup ";
            $op = 0;
        }
    }
}
// * kondisi dimana untuk mengecek apakah stok brang ada atau tidak jika tidak maka akan $op akan 0 jika ada maka 
//              $op akan 1 kemudian dilanjutkan pada tugas selanjutnya 
// * comment baris a berfungsi untuk mengupdate stok barang di tabel barang yaitu dengan mengurangi stok 
// * comment baris b adalah query untuk mengupdate tabel detail_transaksi karena untuk mengupdate stok yang dimasukkan 
//              kan diupdate pada tabel detail_transaksi yang diupdate adalah stok 
// * comment baris c adalah query untuk mendapatkan data detail_transaksi yang wherenya id transaksi oleh karena itu bisa 
//              kemungkinan muncul banyak tergantung berapa barang yang dimasukkan digunakan untuk mendapatkan jumlah akhir 
// * comment barid d adlah query yang digunakan untuk mengambil data diskon pada tabel transaksi digunakan untuk mengurangi 
//              dan akan mengupdate pada tabel transaksi
// * comment baris e adalah query untuk mengupdate data grantotal semua dan telah dikurangi oleh diskon 

if ($op == 1) { // baris kondisi 
    $sql = "update barang set brg_stok = ((brg_stok + $dt_kuantitas_lama)-$dt_kuantitas) where brg_id = '$dt_id_barang'; ";
    $conn->query($sql); // baris a
    
    $sql = "update detail_transaksi set dt_id_barang = '$dt_id_barang', dt_diskripsi = '$dt_diskripsi',
            dt_harga = '$dt_harga', dt_kuantitas = '$dt_kuantitas', dt_jumlah = '$dt_jumlah'  
            where dt_id = '$dt_id'; ";
    $conn->query($sql); // baris b 
    

    $sql = "SELECT * FROM detail_transaksi
            where dt_idtran = ".$tran_id;
        $result = $conn->query($sql); // baris c
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $jumlah = $jumlah + $row["dt_jumlah"];
            }
        }
        $jumlahsubtotal = $jumlah;
    
        $sql = "SELECT * FROM transaksi
                where tran_id = ".$tran_id;
        $result = $conn->query($sql); // baris d 
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $diskon = $row["tran_diskon"];
            }
        }
    
        $grandtotal = $jumlahsubtotal - $diskon;
    
        $sql = "update transaksi set tran_grandtotal = '$grandtotal' where tran_id = '$tran_id'; ";
        $conn->query($sql); // baris e
        $pesan = "Data Berhasil diubah";
}


?>
<!-- digunakan untuk pesan pada user bahwa update yang dia inginkan apakah bisa diubah atau tidak -->
<script> alert ('<?php echo $pesan;?>');
window.location.assign("indexTran.php"); </script>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "projek";

$conn = new mysqli($servername, $username, $password, $dbname);

// untuk mendapatkan get dari form hapus 
$tran_id = $_GET["tran_id"];

// * baris kondisi dimana mengecek apakah id yang dikirim kosong atau tidak , ketika kosong tidak akan diproses oleh sistem 
// * baris query dimana digunakan untuk menghapus row yang ada di tabel transaksi, jika berhasil akan menampilkan pesan 
//              sukses jika tidak berhasil untuk menghapus akan keluar pesan error
// * baris else digunakan untuk ketika id transaksi tidak ada maka akan langsung ada pesan bahwa hapus terjadi masalah 
//              silahkan ulangi kembali 
if($tran_id != null ){ // baris kondisi
    $sql = "DELETE FROM transaksi WHERE tran_id = '$tran_id';";
    if ($conn->query($sql) === TRUE) { // baris query 
        $pesan ="Data Berhasil dihapus"; 
        $sql = "DELETE FROM detail_transaksi WHERE dt_idtran = '$tran_id';";
        $conn->query($sql);
    } else {
        $pesan = "Error: " . $sql . "<br>" . $conn->error;
    }
    
}else { // baris else 
    $pesan = 'Hapus Terjadi Masalah silahkan ulangi kembali';
}
?>

<!-- digunakan untuk menampilkan pesan dan redirect -->
<script>
alert ('<?php echo $pesan;?>');
window.location.assign("../transaksi/indexTran.php");
</script>
<?php
// * file ini digunakan untuk menampilkan data detail pada suatu transaksi dan menampilkan data barang yang ada ditransaksi tersebut 

// * created_date : 2018-12-28
// * update_date : 2019-01-03

include('../koneksi/conn.php');
include('../header.php');

// digunakan untuk menangkap GET data dari transaksi 
$tran_id = $_GET["tran_id"];

// query yang digunakan adalah untuk menampilkan semua detail transaksi pada transaksi tertentu
$sql = "SELECT * 
        FROM detail_transaksi
        JOIN transaksi ON dt_idtran = tran_id 
        JOIN barang ON dt_id_barang = brg_id
        WHERE brg_status = 1
        AND tran_id = '$tran_id'; ";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) { 
          $tran_kode = $row["tran_kode"];  
          $tran_id = $row["tran_id"];  
          $data[] = $row;
      }
  }

//   print_r ($data);
?>



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Manajemen Data Transaksi (<?php echo $tran_kode; ?>)
            <small>Data transaksi</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Home</a>
            </li>
            <li>
                <a href="#">Tables</a>
            </li>
                                        
            <li class="active">Data transaksi </li> 
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Transaksi</h3>
                        <a href="invoice.php?tran_id=<?php echo $tran_id; ?>" class="btn btn-primary">Invoice</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Barang</th>
                                    <th>Deskripsi</th>
                                    <th>Harga</th>
                                    <th>Kuantitas</th>
                                    <th>Jumlah</th>
                                    <th>Edit</th>
                                    <th>Hapus</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i=1;
                                    foreach($data as $row ) { ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $row["brg_nama"];?></td>
                                    <td><?php echo $row["dt_diskripsi"];?></td>
                                    <td><?php echo "Rp " . number_format($row["dt_harga"],2,',','.');?></td>
                                    <td><?php echo $row["dt_kuantitas"];?></td>
                                    <td><?php echo "Rp " . number_format($row["dt_jumlah"],2,',','.');?></td>
                                    <td>
                                        <a href="javascript:isidata('<?php echo $row["dt_id"];?>','<?php echo $row["brg_id"];?>','<?php echo $row["dt_diskripsi"];?>','<?php echo $row["dt_harga"];?>','<?php echo $row["dt_kuantitas"];?>','<?php echo $row["dt_jumlah"];?>');">
                                            <button data-toggle="modal" data-target="#exampleModal" type="submit" class="btn btn-warning">Edit</button>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="javascript:hapus('<?php echo $row["brg_id"];?>');" class="btn btn-danger">hapus</a>
                                    </td>   
                                </tr>
                                <?php
                                $i++;
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
<script>
// * digunakan untuk datatable 
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
    // * fungsi yang digunakan untuk menghapus detail barang akan tetapi dikonfirmasi dahulu apakah salah pencet atau benar 
    //          akan dihapus 
    function hapus(id) {
        var msg = confirm("Apakah Anda yakin dihapus?");
        if (msg == true) {
            window.location = "barang/hapus.php?brg_id=" + id;
        }
    }
    // * fungsi yang digunakan untuk  
    function isidata(id_detail,dt_id_barang,dt_diskripsi,dt_harga,dt_kuantitas,dt_jumlah){
        $("#dt_id").val(id_detail);
        $("#dt_id_barang").val(dt_id_barang);
        $("#dt_diskripsi").val(dt_diskripsi);
        $("#dt_harga").val(dt_harga);
        $("#dt_kuantitas").val(dt_kuantitas);
        $("#dt_kuantitas_lama").val(dt_kuantitas);
        $("#dt_jumlah").val(dt_jumlah);
    }

    
    function hitungjumlahproduct() {
        var dt_harga = $("#dt_harga").val();
        var dt_kuantitas = $("#dt_kuantitas").val();
        var jumlah = parseInt(dt_harga * dt_kuantitas);
        $("#dt_jumlah").val(convertToRupiah(jumlah));
    }
    function convertToRupiah(angka) {
        var rupiah = '';
        var angkarev = angka
            .toString()
            .split('')
            .reverse()
            .join('');
        for (var i = 0; i < angkarev.length; i++) 
            if (i % 3 == 0) 
                rupiah += angkarev.substr(i, 3) + '.';
    return 'Rp. ' + rupiah
            .split('', rupiah.length - 1)
            .reverse()
            .join('');
    }
</script>

<?php
include('../footer.php');
?>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="editdetail.php">
        <input type="text" name="dt_id" id="dt_id">
        <input type="text" name="tran_id" id="tran_id" value="<?php echo $tran_id;?>">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nama Barang</label>
            <select id="example" class="form-control" name="dt_id_barang" id="dt_id_barang">
                <?php
                $sql = "SELECT * FROM barang where brg_status = 1";
                $result = $conn->query($sql);
                $i=1;
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) { ?>
                <option value="<?php echo $row["brg_id"];?>"><?php echo $row["brg_nama"];?></option>

                <?php
                $i++;
                    }
                }
                ?>
            </select>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Deskripsi</label>
            <input type="text" class="form-control" id="dt_diskripsi" name="dt_diskripsi">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Harga</label>
            <input type="text" class="form-control" id="dt_harga" name="dt_harga" onkeyup="javascript:hitungjumlahproduct()">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Kuantitas</label>
            <input type="text" class="form-control" id="dt_kuantitas" name="dt_kuantitas" onkeyup="javascript:hitungjumlahproduct()" >
            <input type="hidden" id="dt_kuantitas_lama" name="dt_kuantitas_lama">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Jumlah</label>
            <input type="text" class="form-control" id="dt_jumlah"  name="dt_jumlah" readonly>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Send message</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php
include('../koneksi/conn.php');
include('../header.php');

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Manajemen Data Transaksi
            <small>Data transaksi</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Home</a>
            </li>
            <li>
                <a href="#">Tables</a>
            </li>
            <li class="active">Data transaksi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Transaksi</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Customer</th>
                                    <th>Kode Transaksi</th>
                                    <th>Tanggal</th>
                                    <th>Sub Total</th>
                                    <th>Diskon</th>
                                    <th>Grand Total</th>
                                    <th>Edit</th>
                                    <th>Hapus</th>
                                    <!-- <th></th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sql = "SELECT * FROM transaksi
                                        join client on cli_id = tran_idcli
                                        where cli_status = 1
                                        order by tran_id asc";
                                $result = $conn->query($sql);
                                $i=1;
                                if ($result->num_rows > 0) {
                                    while($row = $result->fetch_assoc()) { ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $row["cli_nama"];?></td>
                                    <td><?php echo $row["tran_kode"];?></td>
                                    <td><?php echo $row["tran_date"];?></td>
                                    <td><?php echo "Rp " . number_format($row["tran_subtotal"],2,',','.');?></td>
                                    <td><?php echo "Rp " . number_format($row["tran_diskon"],2,',','.');?></td>
                                    <td><?php echo "Rp " . number_format($row["tran_grandtotal"],2,',','.');?></td>
                                    <td>
                                        <form action="inputTran.php" method="post">
                                            <input type="hidden" name="tran_id" value="<?php echo $row["tran_id"];?>">
                                            <button type="submit" class="btn btn-warning">Edit</button>
                                        </td>
                                    </form>
                                    <td>
                                        <a href="javascript:hapus('<?php echo $row["tran_id"];?>');" class="btn btn-danger">hapus</a>
                                    </td>
                                    <!-- <td>
                                        <a href="indexDetailTran.php?tran_id=<?php echo $row["tran_id"];?>" class="btn btn-primary">Detail</a>
                                    </td> -->
                                </tr>
                                <?php
                                $i++;
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
<script>
$(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    })
})

function hapus(id) {
    var msg = confirm("Apakah Anda yakin dihapus?");
    if (msg == true) {
        window.location = "hapus.php?tran_id="+id;
    }
}
</script>

<?php
include('../footer.php');
?>
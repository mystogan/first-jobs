<?php
// file ini untuk input trabsaksi dan digunakan untuk input barang2 yang dinamis untuk menambahkan 
// created_date : 2018-12-28
// updated_date : 2019-01-04
include('../koneksi/conn.php');

// berfungsi untuk mendapatkan method POST yang telah dikirim oleh form 
$tran_id = $_POST["tran_id"];

// query yang digunakan untuk mengambil data dari tabel transaksi berguna untuk menampilkan data yang akan diedit 

$sql = "SELECT * FROM transaksi where tran_id = '$tran_id'";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        // metode untuk penyimpanan pada variabel digunakan untuk mengecho pada waktu di input 
        $tran_idcli = $row["tran_idcli"];  
        $tran_kode = $row["tran_kode"];  
        $tran_date = $row["tran_date"];  
        $tran_subtotal = $row["tran_subtotal"];
        $tran_diskon = $row["tran_diskon"];
        $tran_grandtotal = $row["tran_grandtotal"];
      }
  }
//   * query untuk mengambil detail transaksi dengan id transaksi yang telah dikirim oleh user, 
//                  dalam query ini mengGet semua detail_transaksi yang ada dengan id transaksi 
  $sql = "SELECT * FROM detail_transaksi where dt_idtran = '$tran_id' and dt_status = 1 ;";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
      $i=0;
      while($row = $result->fetch_assoc()) {
          // menyimpan dengan array karena hasil dari detail transaksi bisa lebih dari 1 , mulai indexnya dari $i = 0
          $data[$i]['dt_id'] = $row["dt_id"];
          $data[$i]['dt_id_barang'] = $row["dt_id_barang"];
          $data[$i]['dt_diskripsi'] = $row["dt_diskripsi"];
          $data[$i]['dt_harga'] = $row["dt_harga"];
          $data[$i]['dt_kuantitas'] = $row["dt_kuantitas"];
          $data[$i]['dt_jumlah'] = $row["dt_jumlah"];
          $i++;
      }
  }
//  * kondisi dimana untuk mengecek bahwa apakah data detail_transaksi ada atau tidak karena berkaitan 
//              untuk menghitung index brang yang bisa ditambahkan
  if($data == null)
  { 
    //   mendeklarasikan data = 1 berguna untuk mengset bahwa jika detail_transaksi tidak ada maka akan di set 1 oleh sistem 
    $data = 1;
  }
  // dan ini adalah menyimpan variabel dalam hitung dan akan di echo di inputan 
  $hitung = count($data);
// berfungsi untuk memanggil header.php untuk menampilkan 
  include('../header.php');
?>

<link
    rel="stylesheet"
    href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css">
<script src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Input Transaksi
            <small>barang</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Home</a>
            </li>
            <li>
                <a href="#">Forms</a>
            </li>
            <li class="active">Input Transaksi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Input Transaksi</h3>
                    </div>
                    <form role="form" action="javascript:input();" id="input" method="POST">
                        <input type="hidden" value="<?php echo $hitung;?>" id="hitung">
                        <input type="hidden" name="tran_id" value="<?php echo $tran_id;?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nama Customer
                                    <span style="color:red;">*</span></label><br>
                                <select id="example" class="form-control" style="width:30%" name="tran_idcli">
                                    <?php
                                    // proses untuk melihat semua client yang ada didatabase untuk memudahkan admin memilih 
                                    //              users maka bisa otomatis bisa disearch 
                                  $sql = "SELECT * FROM client where cli_status = 1";
                                  $result = $conn->query($sql);
                                  $i=1;
                                  if ($result->num_rows > 0) {
                                      while($row = $result->fetch_assoc()) { ?>
                                    <option
                                        value="<?php echo $row["cli_id"];?>"
                                        <?php if($row["cli_id"] == $tran_idcli){ echo "selected"; } ?>><?php echo $row["cli_nama"];?></option>

                                    <?php
                                  $i++;
                                      }
                                  }
                                  ?>
                                </select>
                                <script>
                                    // digunakan untuk mendeklarasikan bahwa ketika dropdown client bisa melalui
                                    // ketik/an
                                    $('#example').select2({placeholder: 'Select a User'});
                                </script>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Kode Transaksi
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    style="width:30%"
                                    name="tran_kode"
                                    value="<?php echo $tran_kode;?>"
                                    id="exampleInputPassword1"
                                    required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Tanggal Transaksi
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="date"
                                    class="form-control"
                                    style="width:30%"
                                    value="<?php echo $tran_date;?>"
                                    name="tran_date"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan Satuan Barang"
                                    required="required">
                            </div>
                            <div id="form_tambah">
                            <?php 
                                // perulangan yang digunakan untuk mengetahui jumlah detail_transaksi yang ada yang 
                                //              sebelumnya sudah di dapat melalui database 
                                for ($j=0; $j < count($data); $j++) {
                                    // * keterangan $j adalah index yang digunakan untuk mengset index pada masing-masing detail transaksi 
                                    // * kondisi dimana untuk memisahkan bahwa index ke 0 atau barang ke 1 tidak boleh dihapus dan tidak boleh keluar hapusnya
                                    // * baris a berfungsi untuk mendeklarasikan hanya index ke 0 yang tidak dtampilkan hapusnya 
                                    // * baris b berfungsi supaya index ke 0 tidak bisa melakukan click 
                                    // * baris c adlaha untuk index selanjutnya maka wajib untuk menampilkan button hapus 
                                    // * baris d adlah untuk mengset supaya onclick di button bisa di click 
                                    if($j == 0){
                                        $display = 'none'; // baris a 
                                        $href = "#"; // baris b 
                                    }else {
                                        $display = 'block'; // baris c
                                        $href = "javascript:hapus('$j')"; // baris d
                                    }
                                    ?>
                                <div class="form-group col-md-12 form_hehe" id="form_hehe-<?php echo $j;?>">
                                    <div class="form-group col-md-2">
                                        <label for="exampleInputFile">Barang
                                            <span style="color:red;">*</span></label>
                                        <select
                                            name="dt_id_barang[]"
                                            id="brg_id_mas-<?php echo $j;?>"
                                            class="form-control brg_id_mas"
                                            onchange="javascript:getBarang(<?php echo $j;?>);">
                                            <div id="select_id1">
                                                <?php
                                                // * mencari data barang yang ada didatabase digunakan untuk ditampilakn pada admin utnuk memilih barang tesebut
                                          $sql = "SELECT * FROM barang where brg_status = 1";
                                          $result = $conn->query($sql);
                                          $i=1;
                                          if ($result->num_rows > 0) {
                                              while($row = $result->fetch_assoc()) { ?>
                                                <option
                                                    value="<?php echo $row["brg_id"];?>"
                                                    <?php if($row["brg_id"] == $data[$j]['dt_id_barang']){ echo "selected"; }?>><?php echo $row["brg_nama"];?></option>
                                                <?php
                                          $i++;
                                          $totalBarang = $i;
                                              }
                                          }
                                          ?>
                                            </div>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="exampleInputFile">Deskripsi</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="dt_diskripsi[]"
                                            id="dt_deskripsi"
                                            value="<?php echo $data[$j]['dt_diskripsi'];?>">
                                    </div>
                                    <div class="form-group col-md-2" id="get-harga-<?php echo $j;?>">
                                        <label for="exampleInputFile">Harga</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="dt_harga[]"
                                            id="dt_harga-<?php echo $j;?>"
                                            value="<?php echo $data[$j]['dt_harga'];?>"
                                            onkeyup="javascript:hitungjumlahproduct(<?php echo $j;?>);">
                                    </div>
                                    <div class="form-group col-md-2" id="get-kuantitas-<?php echo $j;?>">
                                        <label for="exampleInputFile">Kuantitas</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="dt_kuantitas[]"
                                            value="<?php echo $data[$j]['dt_kuantitas'];?>"
                                            id="dt_kuantitas-<?php echo $j;?>"
                                            onkeyup="javascript:hitungjumlahproduct(<?php echo $j;?>);">
                                    </div>
                                    <div class="form-group col-md-2" id="get-jumlah-<?php echo $j;?>">
                                        <label for="exampleInputFile">Jumlah</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="dt_jumlah-<?php echo $j;?>"
                                            readonly="readonly"
                                            value="<?php echo "Rp. ".number_format($data[$j]['dt_jumlah'],2,',','.');?>">
                                    </div>
                                    <div class="form-group col-md-2 text" id="get-hapus-<?php echo $j;?>">
                                        <br>
                                        <a
                                            href="<?php echo $href;?>"
                                            class="btn btn-danger"
                                            style="display: <?php echo $display;?>;">Hapus</a>
                                    </div>
                                </div>
                                <?php
                                }
                                ?>

                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-group col-md-2">
                                    <a href="javascript:tambahform();" class="btn btn-primary">Tambah Barang</a>
                                </div>
                            </div>
                            <div id="form_tambah2"></div>
                            <div class="form-group">
                                <label for="exampleInputFile">Jumlah
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    value="<?php echo "Rp. ".number_format($tran_subtotal,2,',','.');?>"
                                    style="width:30%"
                                    id="fulljumlah"
                                    readonly="readonly">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Diskon
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    style="width:30%"
                                    value="<?php echo $tran_diskon;?>"
                                    name="tran_diskon"
                                    id="tran_diskon"
                                    onkeyup="javascript:hitungjumlah()"
                                    required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Grand Total
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    value="<?php echo "Rp. ".number_format($tran_grandtotal,2,',','.');?>"
                                    class="form-control"
                                    style="width:30%"
                                    id="grandjumlah"
                                    readonly="readonly">
                            </div>

                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- page script -->
<?php
include('../footer.php');
?>

<script>
    let totalBarang = <?php echo $totalBarang-1;?>;
    let cektotalBarang = $("#hitung").val();
    // * fungsi untuk memanggil datatable yang include dengan plugins
    $(function () {
        $('#example1').DataTable()
        hitungjumlah();

        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })

    // * function untuk menghitung jumlah produk yang dibeli dan harga yang akan
    // dibayarkan dimana      parameternya kondisional berada pada index berapa
    // * argumen pertama untuk mengambil data harga untuk barang ke i
    // * argumen kedua adalah untuk mengambil kuantitas untuk barang ke i
    // * argumen ketiga adalah mengkalikan untuk mendapatkan harga total (harga
    // barang x jumlah barang)
    // * untuk mengset kedalam inputan harga yang 'readonly' dan dipanggil fungsi
    // convertrupiah untuk merubah      formatkedalam format RUPIAH
    // * fungsi hitungjumlah() akan dijelaskan dibawahnya
    //
    function hitungjumlahproduct(id) {
        var dt_harga = $("#dt_harga-" + id).val();
        var dt_kuantitas = $("#dt_kuantitas-" + id).val();
        var jumlah = parseInt(dt_harga * dt_kuantitas);
        $("#dt_jumlah-" + id).val(convertToRupiah(jumlah));
        hitungjumlah();
    }

    // * fungsi hitungjumlah untuk menghitung total belanja seluruhnya dan
    // menghitung hasil pengurangan diskon          yang sudah diinputkan
    // * untuk variable hitung untuk mengambil value untuk hitungan untuk index
    // ketika diklik tambah
    // * untuk variable dt_harga deklarasi untuk menyimpan value dari input harga
    // berdasarkan index
    // * untuk variable dt_kuantitas, deklarasi untuk menyimpan value dari inpue
    // kuantitas berdasarkan index
    // * variable fulljumlah adalah untuk menghitung semua hasil kali dari harga dan
    // kuantitas atau jumlah barang yang dibeli
    // * untuk perulangan digunakan untuk mengulang semua inputan barang yang sudah
    // diinputkan jadi contohnya          jika jumlah barangnya 5 itu akan disimpan
    // di variable hitung dan akan diulang sampai jumlah hitung
    // * kemudian untuk baris kondisi untuk kondisi dimana inputan tidak null atau
    // tidak ada isinya atau sudah dihapus          untuk antisipasi ketia ada
    // barang yang sudah dihapus tidak akan dihitung
    // * untuk comment baris a digunakan untuk mengkalikan harga dan jumlah barang
    // yang akan dibeli per barang
    // * utnuk comment baris b berfungsi untuk menghitung jumlah barang belanjaan
    // semua barang
    // * untuk comment baris c digunakan untuk mengset dan mengkonversi dari int ke
    // format rupiah dan mengset ke inputan
    // * untuk comment baris d berfungsi untuk mengambil value dari diskon
    // * untuk comment baris e digunakan untuk mengset dan mengkonversi dari int ke
    // format rupiah dan mengset ke inputan total jumlah belanja
    function hitungjumlah() {
        var hitung = $("#hitung").val();
        var dt_harga;
        var dt_kuantitas;
        var fullJumlah = 0;
        for (let index = 0; index < hitung; index++) { // baris perulangan
            dt_harga = $("#dt_harga-" + index).val();
            dt_kuantitas = $("#dt_kuantitas-" + index).val();
            if (dt_harga != null) { // baris untuk kondisi
                var jumlah = parseInt(dt_harga * dt_kuantitas); // baris a
                fullJumlah = parseInt(fullJumlah + jumlah); // baris b
            }

        }
        $("#fulljumlah").val(convertToRupiah(fullJumlah)); // baris c
        var tran_diskon = $("#tran_diskon").val(); // baris d
        $("#grandjumlah").val(convertToRupiah(fullJumlah - tran_diskon)); // baris e

    }

    // * fungsi ini berfungsi untuk mengubah format int kedalam format rupiah
    // Indonesia
    // * pada variable rupiah digunakan untuk deklarasi
    // * untuk baris perulangan digunakan untuk mengulang jumlah angka yang
    // diparameterkan jadi contohnya jika          angka tersebut sudah berulang ke
    // 3 kemudian dikasih '.' diantaranya  ex 1000 => 1.000 dan seterusnya
    // * baris return adalah sebuah kembalian nilai , jadi nilai yang dikembalikan
    // kepada sipemanggil fungsi tersebut
    function convertToRupiah(angka) {
        var rupiah = '';
        var angkarev = angka
            .toString()
            .split('')
            .reverse()
            .join('');
        for (var i = 0; i < angkarev.length; i++) // baris perulangan
            if (i % 3 == 0) // baris kondisi
                rupiah += angkarev.substr(i, 3) + '.';
    return 'Rp. ' + rupiah // baris return
            .split('', rupiah.length - 1)
            .reverse()
            .join('');
    }

    // * fungsi ini berfungsi untuk mengambil harga barang di database dan mengset
    // kedalam inputan
    // * untuk comment baris a berfungsi untuk mengambil value dari id barang yang
    // ke i dalam parameter
    // * untuk baris b berfungsi untuk ajax yang digunakan untuk load ke database
    // tanpa reload
    // * utnuk baris c berfungsi untuk link yang dituju dan menyertakan parameter
    // yang akan ditambah
    // * untuk baris d adalah return atau kembaliam yang diterima dari function
    // sebelumnya untuk diset kedalam inputan harga barang
    function getBarang(id) {
        var brg_id = $("#brg_id_mas-" + id).val(); // baris a
        let hitung = $("#hitung").val();
        let kurang = parseInt(brg_id - 1);
        
        $.ajax({ // baris b
            type: "GET",
            url: "<?php echo $url;?>transaksi/cekbarang.php?brg_id=" + brg_id, // baris c
            success: function (data) {
                var obj = JSON.parse(data);
                $("#dt_harga-" + id).val(obj['brg_hargajual']); // baris d
            }
        });
    }
    // berfungsi untuk ajax yaitu admin tidak peru reload untuk menyimpan data yang
    // akan disimpan , maka ajax akan mengecek              bahwa apakah barang yang
    // ada masih tersedia atau bisa disimpan atau tidak bisa disimpan , maka jika
    // kembali              akan otomatis diberi pesan saja tidak akan reload jadi
    // admin tidak perlu capek untuk menginput barang yang banyak
    function input(id) {
        var brg_id = $("#brg_id_mas-" + id).val(); // baris a
        $.ajax({ // baris b
            type: "POST",
            url: "<?php echo $url;?>transaksi/input.php", // baris c
            data: $('#input').serialize(),
            success: function (data) {
                // let json  = JSON.stringify(data) alert (json)
                var obj = JSON.parse(data);
                if (obj['cek'] == 0) {
                    alert(obj['pesan'])
                } else {
                    alert("Data tersimpan")
                    window.location.href = 'indexTran.php';
                }
            }
        });
    }

    // * fungsi ini berfungsi untuk  menambahkan inputan barang baru dan mengedit
    // inputan seperti id dan onclick dll
    // * comment baris a berfungsi untuk mengambil value dari index hitung atau
    // jumlah barang yang telah ditambahkan
    // * comment baris b adalah berfungsi untuk mengurangi index dengan 1 bertujuan
    // untuk mengset div setelah diclone              karena setelah clone hitung
    // akan ditambah satu jadi ketika clone id yang di clone adalah kurang dari 1
    // dari index
    // * cooment baris c adlaah fungsi mengclone satu div yang berkaitan dalam hal
    // ini adalah form barang yang akan diclone
    // * comment baris d adlah mengganti id dari id yang telah diclonning sehingga
    // bisa diambil value yang akan digunakan
    // * comment baris e adalah berfungsi untuk menambahkan dalam div tersebut dalam
    // hal ini adalah id div form_tambah akan              ditambahkan form_hehe-(i)
    // untuk selanjutnya
    // * comment baris f adalah deklarasi untuk dif yang beridkan tersebut digunakan
    // untuk mencari id yang ada di id yang dideklarasikan tersebut
    // * comment baris g adalah untuk mengganti  id dalam html select menjadi id
    // yang sudah dihitung indexnya
    // * comment baris h adalah untuk menambahkan fungsi onchange dan memanggil
    // fungsi javascript dalam hal ini adalah              getbarang(index) yang
    // sudah dijelaskan pada comment diatas
    // * comment baris i adlaah mencari div id dalam div bertujuan untuk mengganti
    // id yang sudah diclone
    // * comment baris j berfungsi untuk mengganti id yang sudah dicari sebelumnya
    // * comment baris k adalah untuk mencari id dan mengganti
    // * comment baris l adalah untuk menambah index penambahan barang supaya barang
    // bertambah sesuai dengan index yang digunakan
    // * commment baris m adalah untuk mengset hitungan yang sudah ditambah ke
    // inputan hitung tersebut karena untuk mengset hitung
    function tambahform() {
        var hitung = $("#hitung").val(); // baris a
        // alert (hitung);
        if (totalBarang == cektotalBarang) {
            alert("Jumlah Row Sudah Sama dengan Jumlah Barang . Silahkan di Isi");
        } else {
            cektotalBarang++;
            let kurang = parseInt(hitung - 1) // baris b
            var newRow = $("#form_hehe-" + kurang).clone() // baris c
            newRow.attr('id', 'form_hehe-' + hitung) // baris d
            newRow.appendTo("#form_tambah"); // baris e
            var newFirst = $("#form_hehe-" + hitung) // baris f
            newFirst
                .find('select')
                .attr('id', 'brg_id_mas-' + hitung) // baris g
            newFirst
                .find('select')
                .attr('onchange', "javascript:getBarang('" + hitung + "')") // baris h
            var newHarga = newFirst.find('div#get-harga-' + kurang) // baris i
            newHarga.attr('id', 'get-harga-' + hitung) // baris j
            newHarga
                .find('input')
                .attr('id', 'dt_harga-' + hitung) // baris k
            newHarga
                .find('input')
                .attr('onkeyup', "javascript:hitungjumlahproduct('" + hitung + "')")
            $('#dt_harga-' + hitung).val('')
            var newKuantitas = newFirst.find('div#get-kuantitas-' + kurang)
            newKuantitas.attr('id', 'get-kuantitas-' + hitung)
            newKuantitas
                .find('input')
                .attr('id', 'dt_kuantitas-' + hitung)
            newKuantitas
                .find('input')
                .attr('onkeyup', "javascript:hitungjumlahproduct('" + hitung + "')")
            $('#dt_kuantitas-' + hitung).val('')
            var newJumlah = newFirst.find('div#get-jumlah-' + kurang)
            newJumlah.attr('id', 'get-jumlah-' + hitung)
            newJumlah
                .find('input')
                .attr('id', 'dt_jumlah-' + hitung)
            $('#dt_jumlah-' + hitung).val('')
            var newHapus = newFirst.find('div#get-hapus-' + kurang)
            newHapus.attr('id', 'get-hapus-' + hitung)
            newHapus
                .find('a')
                .attr('href', "javascript:hapus('" + hitung + "')")
            newHapus
                .find('a')
                .attr('style', "display: block;")
            var tambah = parseInt(hitung) + 1; // baris l
            $("#hitung").val(tambah) // baris m

        }

    }
    // * fungsi ini digunakan untuk mengahpus barang yang dianggap tidak perlu
    // ketiak terlanjur menambahkan
    // * comment baris a adalah untuk menghapus barang berindex ke i yang didapatkan
    // pada parameter
    // * baris b digunakan untuk memanggil fungsi hitungjumlah() yang sudah
    // dijelaskan pada comment sebelumnya
    function hapus(id) {
        document
            .getElementById("form_hehe-" + id)
            .remove(); // baris ke a
        hitungjumlah(); // baris b
        cektotalBarang--;
    }
</script>
<?php
include('../koneksi/conn.php');
include('../header.php');
?>

<link
    rel="stylesheet"
    href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css">
<script src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Input Transaksi
            <small>barang</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Home</a>
            </li>
            <li>
                <a href="#">Forms</a>
            </li>
            <li class="active">Input Transaksi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Input Transaksi</h3>
                    </div>
                    <form role="form" action="input.php" method="POST">
                        <input type="hidden" value="1" id="hitung">
                        <input type="hidden" name="tran_id" value="">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nama Customer
                                    <span style="color:red;">*</span></label><br>
                                <select id="example" class="form-control" style="width:30%" name="tran_idcli">

                                    <?php
                                  $sql = "SELECT * FROM client where cli_status = 1";
                                  $result = $conn->query($sql);
                                  $i=1;
                                  if ($result->num_rows > 0) {
                                      while($row = $result->fetch_assoc()) { ?>
                                    <option value="<?php echo $row["cli_id"];?>"><?php echo $row["cli_nama"];?></option>

                                    <?php
                                  $i++;
                                      }
                                  }
                                  ?>
                                </select>
                                <script>
                                    $('#example').select2({placeholder: 'Select a month'});
                                </script>
                                <!-- <input type="text" class="form-control" style="width:30%" name="tran_idcli"
                                id="exampleInputPassword1" required="required"> -->
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Kode Transaksi
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    style="width:30%"
                                    name="tran_kode"
                                    id="exampleInputPassword1"
                                    required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Tanggal Transaksi
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="date"
                                    class="form-control"
                                    style="width:30%"
                                    name="tran_date"
                                    id="exampleInputPassword1"
                                    placeholder="Masukkan Satuan Barang"
                                    required="required">
                            </div>
                            <div id="form_tambah">
                                <div class="form-group col-md-12 form_hehe" id="form_hehe-0">
                                    <div class="form-group col-md-2">
                                        <label for="exampleInputFile">Barang
                                            <span style="color:red;">*</span></label>
                                        <select
                                            name="dt_id_barang[]"
                                            id="brg_id_mas-0"
                                            class="form-control brg_id_mas"
                                            onchange="javascript:getBarang(0);">
                                            <div id="select_id1">
                                                <?php
                                          $sql = "SELECT * FROM barang where brg_status = 1";
                                          $result = $conn->query($sql);
                                          $i=1;
                                          if ($result->num_rows > 0) {
                                              while($row = $result->fetch_assoc()) { ?>
                                                <option value="<?php echo $row["brg_id"];?>"><?php echo $row["brg_nama"];?></option>

                                                <?php
                                          $i++;
                                              }
                                          }
                                          ?>
                                            </div>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="exampleInputFile">Deskripsi</label>
                                        <input type="text" class="form-control" name="dt_diskripsi[]" id="dt_deskripsi">
                                    </div>
                                    <div class="form-group col-md-2" id="get-harga-0">
                                        <label for="exampleInputFile">Harga</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="dt_harga[]"
                                            id="dt_harga-0"
                                            onkeyup="javascript:hitungjumlahproduct(0);">
                                    </div>
                                    <div class="form-group col-md-2" id="get-kuantitas-0" >
                                        <label for="exampleInputFile">Kuantitas</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="dt_kuantitas[]"
                                            id="dt_kuantitas-0"
                                            onkeyup="javascript:hitungjumlahproduct(0);">
                                    </div>
                                    <div class="form-group col-md-2" id="get-jumlah-0" >
                                        <label for="exampleInputFile">Jumlah</label>
                                        <input type="text" class="form-control" id="dt_jumlah-0" readonly="readonly">
                                    </div>
                                    <div class="form-group col-md-2 text" id="get-hapus-0" >
                                        <br>
                                        <a href="#" class="btn btn-danger" style="display: none;">Hapus</a>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-group col-md-2">
                                    <a href="javascript:tambahform();" class="btn btn-primary">Tambah Barang</a>
                                </div>
                            </div>
                            <div id="form_tambah2"></div>
                            <div class="form-group">
                                <label for="exampleInputFile">Jumlah
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    style="width:30%"
                                    name="tran_diskon"
                                    id="fulljumlah"
                                    value="0"
                                    readonly >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Diskon
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    style="width:30%"
                                    name="tran_diskon"
                                    id="tran_diskon"
                                    value="0"
                                    onkeyup="javascript:hitungjumlah()"
                                    required="required" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Grand Total
                                    <span style="color:red;">*</span></label>
                                <input
                                    type="text"
                                    class="form-control"
                                    style="width:30%"
                                    name="tran_diskon"
                                    id="grandjumlah"
                                    value="0"
                                    readonly >
                            </div>
                            
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- page script -->
<?php
include('../footer.php');
?>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })


    function hitungjumlahproduct(id) {
        var dt_harga = $("#dt_harga-"+id).val();
        var dt_kuantitas = $("#dt_kuantitas-"+id).val();
        var jumlah = parseInt(dt_harga * dt_kuantitas);
        $("#dt_jumlah-"+id).val(convertToRupiah(jumlah));
        hitungjumlah();
    }
    function hitungjumlah(){
        var hitung = $("#hitung").val();
        var dt_harga ;
        var dt_kuantitas ;
        var fullJumlah = 0;
        for (let index = 0; index < hitung; index++) {
            dt_harga = $("#dt_harga-"+index).val();
            dt_kuantitas = $("#dt_kuantitas-"+index).val();
            if (dt_harga != null) {
                var jumlah = parseInt(dt_harga * dt_kuantitas);
                fullJumlah = parseInt(fullJumlah + jumlah);
            }
            
        }
        $("#fulljumlah").val(convertToRupiah(fullJumlah));
        var tran_diskon = $("#tran_diskon").val();
        $("#grandjumlah").val(convertToRupiah(fullJumlah - tran_diskon));

    }
    function convertToRupiah(angka) {
        var rupiah = '';
        var angkarev = angka
            .toString()
            .split('')
            .reverse()
            .join('');
        for (var i = 0; i < angkarev.length; i++) 
            if (i % 3 == 0) 
                rupiah += angkarev.substr(i, 3) + '.';
    return 'Rp. ' + rupiah
            .split('', rupiah.length - 1)
            .reverse()
            .join('');
    }

    function getBarang(id) {
        var brg_id = $("#brg_id_mas-"+id).val();
        $.ajax({
            type: "GET",
            url: "<?php echo $url;?>transaksi/cekbarang.php?brg_id=" + brg_id,
            success: function (data) {
                var obj = JSON.parse(data);
                $("#dt_harga-"+id).val(obj['brg_hargajual']);
            }
        });
}

    function tambahform() {
        var hitung = $("#hitung").val();
        // alert (hitung);
        var newRow = $("#form_hehe-0").clone()
        newRow.attr('id', 'form_hehe-'+hitung)
        newRow.find('select').attr('id', 'brg_id_mas-'+hitung)
        newRow.find('select').attr('onchange', "javascript:getBarang('"+hitung+"')")
        newRow.prependTo("#form_tambah");
        var newHarga = $("#get-harga-0")
        newHarga.attr('id', 'get-harga-'+hitung)
        newHarga.find('input').attr('id', 'dt_harga-'+hitung)
        newHarga.find('input').attr('onkeyup', "javascript:hitungjumlahproduct('"+hitung+"')")
        $('#dt_harga-'+hitung).val('')
        var newKuantitas = $("#get-kuantitas-0")
        newKuantitas.attr('id', 'get-kuantitas-'+hitung)
        newKuantitas.find('input').attr('id', 'dt_kuantitas-'+hitung)
        newKuantitas.find('input').attr('onkeyup', "javascript:hitungjumlahproduct('"+hitung+"')")
        $('#dt_kuantitas-'+hitung).val('')
        var newJumlah = $("#get-jumlah-0")
        newJumlah.attr('id', 'get-jumlah-'+hitung)
        newJumlah.find('input').attr('id', 'dt_jumlah-'+hitung)
        $('#dt_jumlah-'+hitung).val('')
        var newHapus = $("#get-hapus-0")
        newHapus.attr('id', 'get-hapus-'+hitung)
        newHapus.find('a').attr('href', "javascript:hapus('"+hitung+"')")
        newHapus.find('a').attr('style', "display: block;")
        var tambah = parseInt(hitung)+1;
        $("#hitung").val(tambah)



    }

    function hapus(id) {
        document.getElementById("form_hehe-"+id).remove();
        hitungjumlah();
        // var hitung = $("#hitung").val();
        // var elements = document.getElementsByClassName("form_hehe-"+id);
        // elements[hitung]
        //     .parentNode
        //     .removeChild(elements[hitung]);
    }
</script>
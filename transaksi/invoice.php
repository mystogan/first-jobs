<?php
include('../koneksi/conn.php');

$tran_id = $_GET["tran_id"];

$sql = "SELECT * FROM transaksi 
        join client on cli_id = tran_idcli
        AND tran_id = '$tran_id'; ";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) { 
          $data = $row;
      }
  }

  $sql = "SELECT * FROM transaksi 
            join detail_transaksi on tran_id = dt_idtran
            join barang on dt_id_barang = brg_id
            AND dt_idtran = '$tran_id'; ";
    $result = $conn->query($sql);
        if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) { 
            $detail[] = $row;
        }
    }

//   print_r ($detail);
?>


<link
    href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<section class="content content_content" style="width: 70%; margin: auto;">
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i>
                    Interactive
                    <small class="pull-right">Date:
                        <?php echo date("d-m-Y");?></small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                Customer
                <address>
                    Nama 
                    <br>
                    Perusahaan
                    <br>
                    Alamat
                    <br>
                    Email
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                &ensp;
                <address>
                    <?php echo $data['cli_nama']?>
                    <br>
                    <?php echo $data['cli_perusahaan']?>
                    <br>
                    <?php echo $data['cli_alamat']?>
                    <br>
                    <?php echo $data['cli_email']?>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Invoice #<?php echo $data['tran_kode']?></b>
                <br>
                <br>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama barang</th>
                            <th>Harga</th>
                            <th>Kuantitas</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=1;
                    foreach($detail as $value){ ?>
                        <tr>
                            <td><?php echo ($i++);?></td>
                            <td><?php echo $value['brg_nama']?></td>
                            <td><?php echo "Rp " . number_format($value["dt_harga"],2,',','.');?></td>
                            <td><?php echo $value['dt_kuantitas']?></td>
                            <td><?php echo "Rp " . number_format($value["dt_jumlah"],2,',','.');?></td>
                        </tr>

                    <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>

                            <tr>
                                <th>Diskon:</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><?php echo "Rp " . number_format($value["tran_diskon"],2,',','.');?></td>
                            </tr>
                            <tr>
                                <th>Total:</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><?php echo "Rp " . number_format($value["tran_grandtotal"],2,',','.');?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <!-- <a href="" class="btn btn-default">
                    <i class="fa fa-print"></i>
                    Print</a>
                <button class="btn btn-success pull-right">
                    <i class="fa fa-credit-card"></i>
                    Submit Payment</button>
                <button class="btn btn-primary pull-right" style="margin-right: 5px;">
                    <i class="fa fa-download"></i>
                    Generate PDF</button> -->
            </div>
        </div>
    </section>
</section>
<style>
    .invoice {
        position: relative;
        background: #fff;
        border: 1px solid #f4f4f4;
        padding: 20px;
        margin: 10px 25px;
    }
    .page-header {
        margin: 10px 0 20px;
        font-size: 22px;
    }
</style>

<?php
include('koneksi/conn.php');
include('header.php');

$emp_id = $_POST["emp_id"];

$sql = "SELECT * FROM employee where emp_status = 1 and emp_id = '$emp_id'";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) { 
        $emp_fullname = $row["emp_fullname"];  
        $emp_shortname = $row["emp_shortname"];  
        $emp_jk = $row["emp_jk"];  
        $emp_kotalhr = $row["emp_kotalhr"];  
        $emp_tgllhr = $row["emp_tgllhr"];  
        $emp_alamatktp = $row["emp_alamatktp"];  
        $emp_domisili = $row["emp_domisili"];  
        $emp_hunian = $row["emp_hunian"];  
        $emp_statusnikah = $row["emp_statusnikah"];  
        $emp_anak = $row["emp_anak"];  
        $emp_tlp = $row["emp_tlp"];  
        $emp_hp1 = $row["emp_hp1"];  
        $emp_hp2 = $row["emp_hp2"];  
        $emp_hp3 = $row["emp_hp3"];  
        $emp_pinbb = $row["emp_pinbb"];  
        $emp_email = $row["emp_email"];  
        $emp_devisi = $row["emp_devisi"];  
        $emp_idlokasi = $row["emp_idlokasi"];  
        $emp_jabatan = $row["emp_jabatan"];  
        $emp_level = $row["emp_level"];  
      }
  }

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Input Employee
        <small>Input</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Input Employee</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Employee</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="employee/input.php" method="POST">
            <input type="hidden" name="emp_id" value="<?php echo $emp_id;?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Lengkap Employee  <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="emp_fullname"  value="<?php echo $emp_fullname;?>" id="exampleInputEmail1" placeholder="Masukkan Asal Costumer"  required >
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Panggilan Employee  <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="emp_shortname" value="<?php echo $emp_shortname;?>" id="emp_shortname" placeholder="Masukkan Gabungnya Costumer"  required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Jenis Kelamin Employee</label>
                  <select name="emp_jk" id="emp_jk" class="form-control">
                    <option value="1" <?php if($emp_jk == 1){echo "selected"; }?> >Laki-Laki</option>
                    <option value="2" <?php if($emp_jk == 2){echo "selected"; }?> >Perempuan</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Kota Kelahiran Employee  <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="emp_kotalhr" value="<?php echo $emp_kotalhr;?>" id="emp_kotalhr" placeholder="Masukkan Perusahaan Costumer"   required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Tanggal Lahir Employee  <span style="color:red;">*</span></label>
                  <input type="date" class="form-control" name="emp_tgllhr" value="<?php echo $emp_tgllhr;?>" id="emp_tgllhr" placeholder="Masukkan Bidang Costumer"  required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Alamat KTP  <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="emp_alamatktp" value="<?php echo $emp_alamatktp;?>" id="emp_alamatktp" placeholder="Masukkan NPWP Costumer"  required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Domisili  <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="emp_domisili" value="<?php echo $emp_domisili;?>" id="emp_domisili" placeholder="Masukkan Alamat Costumer"  required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Hunian</label>
                  <input type="text" class="form-control" name="emp_hunian" value="<?php echo $emp_hunian;?>" id="emp_hunian" placeholder="Masukkan Provinsi Costumer">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Status Nikah</label>
                  <select name="emp_statusnikah" id="emp_statusnikah" class="form-control">
                    <option value="1" <?php if($emp_statusnikah == 1){echo "selected"; }?> >Sudah Menikah</option>
                    <option value="2" <?php if($emp_statusnikah == 2){echo "selected"; }?> >Belum Menikah</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Jumlah Anak</label>
                  <select name="emp_anak" id="emp_anak" class="form-control">
                    <option value="0" <?php if($emp_anak == 0){echo "selected"; }?> >0 Anak</option>
                    <option value="1" <?php if($emp_anak == 1){echo "selected"; }?> >1 Anak</option>
                    <option value="2" <?php if($emp_anak == 2){echo "selected"; }?> >2 Anak</option>
                    <option value="3" <?php if($emp_anak == 3){echo "selected"; }?> >3 Anak</option>
                    <option value="4" <?php if($emp_anak == 4){echo "selected"; }?> >4 Anak</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Telepon Employee</label>
                  <input type="text" class="form-control" name="emp_tlp" value="<?php echo $emp_tlp;?>" id="emp_tlp" placeholder="Masukkan HP Costumer">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">No HP 1 Employee</label>
                  <input type="text" class="form-control" name="emp_hp1" value="<?php echo $emp_hp1;?>" id="emp_hp1" placeholder="Masukkan Email Costumer">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">No HP 2 Employee</label>
                  <input type="text" class="form-control" name="emp_hp2" value="<?php echo $emp_hp2;?>" id="emp_hp2" placeholder="Masukkan Email Costumer">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">No HP 3 Employee</label>
                  <input type="text" class="form-control" name="emp_hp3" value="<?php echo $emp_hp3;?>" id="emp_hp3" placeholder="Masukkan Email Costumer">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">PIN BB Employee</label>
                  <input type="text" class="form-control" name="emp_pinbb" value="<?php echo $emp_pinbb;?>" id="emp_pinbb" placeholder="Masukkan Pin BB Costumer">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Email Employee</label>
                  <input type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" name="emp_email" value="<?php echo $emp_email;?>" id="emp_email" placeholder="Masukkan Atasan Costumer">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Devisi</label>
                  <select name="emp_devisi" id="emp_devisi" class="form-control">
                    <option value="1" <?php if($emp_devisi == 1){echo "selected"; }?> >Maketing</option>
                    <option value="2" <?php if($emp_devisi == 2){echo "selected"; }?> >Customer Support</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Lokasi</label>
                  <select name="emp_idlokasi" id="emp_idlokasi" class="form-control">
                  <?php
                    $sql = "SELECT * FROM lokasi where lok_status = 1";
                    $result = $conn->query($sql);
                    $i=1;
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) { ?>
                        <option value="<?php echo $row["lok_id"];?>"  <?php if($emp_idlokasi == $row["lok_id"] ){echo "selected"; }?>  ><?php echo $row["lok_nama"];?></option>
                    
                    <?php
                    $i++;
                        }
                    }
                    ?>

                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Jabatan Employee</label>
                  <input type="text" class="form-control" name="emp_jabatan" value="<?php echo $emp_jabatan;?>" id="emp_jabatan" placeholder="Masukkan Atasan Costumer">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Level</label>
                  <select name="emp_level" id="emp_level" class="form-control">
                    <option value="1" <?php if($emp_level == 1){echo "selected"; }?>  >Administrator</option>
                    <option value="2" <?php if($emp_level == 2){echo "selected"; }?> >Biasa</option>
                  </select>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

<script>
$(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    })
})
</script>

<?php
include('footer.php');
?>
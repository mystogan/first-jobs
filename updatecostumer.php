<?php
include('koneksi/conn.php');
include('header.php');

$cli_id = $_POST["cli_id"];

echo $sql = "SELECT * FROM client where cli_status = 1 and cli_id = '$cli_id'";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) { 
        $cli_from = $row["cli_from"];  
        $cli_gabung = $row["cli_gabung"];  
        $cli_nama = $row["cli_nama"];  
        $cli_perusahaan = $row["cli_perusahaan"];  
        $cli_bidang = $row["cli_bidang"];  
        $cli_npwp = $row["cli_npwp"];  
        $cli_alamat = $row["cli_alamat"];  
        $cli_prov = $row["cli_prov"];  
        $cli_kota = $row["cli_kota"];  
        $cli_telp = $row["cli_telp"];  
        $cli_hp = $row["cli_hp"];  
        $cli_email = $row["cli_email"];  
        $cli_pinBB = $row["cli_pinbb"];  
        $cli_atasan = $row["cli_atasan"];  
        $cli_itconsultan = $row["cli_itconsultan"];  
      }
  }
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Costumer
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">update</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="costumer/input.php" method="POST">
              <div class="box-body">
                  <input type="hidden" name="cli_id" value="<?php echo $cli_id;?>">
                <div class="form-group">
                  <label for="exampleInputEmail1">Tahu Dari</label>
                  <select name="cli_from" id="cli_from" class="form-control">
                    <option value="1" <?php if($cli_from == 1){echo "selected"; }?> >Kebetulan Jalan-Jalan</option>
                    <option value="2" <?php if($cli_from == 2){echo "selected"; }?> >Dimedsos</option>
                    <option value="3" <?php if($cli_from == 3){echo "selected"; }?> >Dari Teman</option>
                    <option value="4" <?php if($cli_from == 4){echo "selected"; }?> >Dari Brosur</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Tanggal Customer Gabung <span style="color:red;">*</span></label>
                  <input type="date" class="form-control" name="cli_gabung" value="<?php echo $cli_gabung;?>" id="exampleInputPassword1" placeholder="Masukkan Gabungnya Costumer" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Nama Customer <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="cli_nama" value="<?php echo $cli_nama;?>" id="exampleInputPassword1" placeholder="Masukkan nama Costumer" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Perusahaan customer <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="cli_perusahaan" value="<?php echo $cli_perusahaan;?>" id="exampleInputPassword1" placeholder="Masukkan Perusahaan Costumer" required  >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Bidang Customer <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="cli_bidang" value="<?php echo $cli_bidang;?>" id="exampleInputPassword1" placeholder="Masukkan Bidang Costumer" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">NPWP Customer <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="cli_npwp" value="<?php echo $cli_npwp;?>" id="exampleInputPassword1" placeholder="Masukkan NPWP Costumer" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Alamat Customer <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="cli_alamat" value="<?php echo $cli_alamat;?>" id="exampleInputPassword1" placeholder="Masukkan Alamat Costumer" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Provinsi Customer</label>
                  <select name="cli_prov" id="cli_prov" class="form-control"  onchange="javascript:cekkota();">
                  <?php
                    $sql = "SELECT * FROM provinces ";
                    $result = $conn->query($sql);
                    $i=1;
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) { ?>
                        <option value="<?php echo $row["id"];?>"  <?php if($cli_prov == $row["id"] ){echo "selected"; }?>  ><?php echo $row["name"];?></option>
                    
                    <?php
                    $i++;
                        }
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Kota Customer</label>
                  <select name="cli_kota" id="cli_kota" class="form-control">
                      <option value="0"  >Silahkan Pilih Provinsi</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Telepon Customer <span style="color:red;">*</span></label>
                  <input type="text" class="form-control" name="cli_telp" value="<?php echo $cli_telp;?>" id="exampleInputPassword1" placeholder="Masukkan Telepon Costumer" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">HP Customer</label>
                  <input type="text" class="form-control" name="cli_hp" value="<?php echo $cli_hp;?>" id="exampleInputPassword1" placeholder="Masukkan HP Costumer">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Email Customer</label>
                  <input type="text" class="form-control" name="cli_email" value="<?php echo $cli_email;?>" id="exampleInputPassword1" placeholder="Masukkan Email Costumer">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">PIN BB Customer</label>
                  <input type="text" class="form-control" name="cli_pinBB" value="<?php echo $cli_pinBB;?>" id="exampleInputPassword1" placeholder="Masukkan Pin BB Costumer">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Atasan Customer</label>
                  <input type="text" class="form-control" name="cli_atasan" value="<?php echo $cli_atasan;?>" id="exampleInputPassword1" placeholder="Masukkan Atasan Costumer">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">IT Consultan Customer</label>
                  <input type="text" class="form-control" name="cli_itconsultan" value="<?php echo $cli_itconsultan;?>" id="exampleInputPassword1" placeholder="Masukkan IT Consultan Costumer">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

<script>
$(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    })
})


function cekkota() {
   var cli_prov = $("#cli_prov").val();
//    alert (cli_prov);
   $("#cli_kota").load("master/kota.php?cli_prov="+cli_prov);
}
</script>

<?php
include('footer.php');
?>